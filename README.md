This project is about Interface configuration cleaning in Cisco ACI. It will allow for static EPG binding removal and Interface Profile cleaning when a givan interface needs to be zeroised.

Scripts (python with Cobra lib's) can be used for cleaning the interface config or simply displaying the current config.

When cleaning the config, user confirmation can be requested.

Jenkings pipeline def are also provided, they require the Active Choices plugin.

Please note that input forms (leaf and port fields) are dynamically filled with a 3rd Jenkins pipeline that must run on regular basis, it will produce artifacts containing your fabric leaf and port inventory.

How it works ?
User provides a leaf id + interface id, associated static EPG bindings and Interface profile (PG wont be removed) are removed.
If the provided interface is member of a PortChannel or VirtualPortchannel the Interface Profile is also removed.

**Take some testing for testing before running in production.**

**My personal testing results are in the wiki**

**Carrefully check and edit jenkins files, update accordingly with your env.**



![](/sources/images/Screenshot_0.jpg)
![](/sources/images/Screenshot_1.jpg)
![](/sources/images/Screenshot_2.jpg)
![](/sources/images/Screenshot_3.jpg)
![](/sources/images/Screenshot_4.jpg)
![](/sources/images/Screenshot_5.jpg)



