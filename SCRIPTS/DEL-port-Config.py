from LIBS.AciLibraries import *
import argparse

# Initialize parser
my_parser = argparse.ArgumentParser(description='A script that will zeroise port config \
    (Access Policies & EPG static binding)')
my_parser.add_argument('-H', '--host', type=str, required=True, metavar='', help='APIC url in x.x.x.x or FQDN format and without https://')
my_parser.add_argument('-U', '--user', type=str, required=True, metavar='', help='APIC username')
my_parser.add_argument('-P', '--password', type=str, required=True, metavar='', help='APIC password')
my_parser.add_argument('-L', '--leaf', type=str, required=True, metavar='', help='Leaf ID')
my_parser.add_argument('-I', '--interface', type=str, required=True, metavar='', help='Interface ID, like eth1/1')
my_parser.add_argument('-C', '--confirm', type=str, required=True, metavar='', choices={"yes", "no"}, help='whether we should prompt user before deleting objects')

args = my_parser.parse_args()


node_id = args.leaf
port_name = args.interface

if args.confirm == "yes":
    user_confirmation = True
else:
    user_confirmation = False

moDir = apic_logon("https://" + args.host, args.user, args.password)


accessPolicies = get_Port_AccessPolicies(moDir=moDir, leaf_id=node_id, port_name=port_name)
if not accessPolicies: # no access policy found
    print("==> No Access Policy Found")
    del_orphan_ports_egp_static_paths(moDir, node_id, port_name, user_confirmation)

elif len(accessPolicies) == 2: # access policy found without PG
    print("==> Polic Group is not set")
    remove_port_access_policies(moDir, interface_selector_DN =accessPolicies[1], port_block_obj = accessPolicies[0], bundle=False, user_confirmation = user_confirmation)
    del_orphan_ports_egp_static_paths(moDir, node_id, port_name, user_confirmation)

else: # access policy found with PG
    if is_pg_bundle(accessPolicies[2]) == True:
        print("==> This is a bundle member port")
        del_port_bundle_epg_static_paths(moDir, accessPolicies[2], user_confirmation)
        remove_port_access_policies(moDir, interface_selector_DN = accessPolicies[1], port_block_obj = accessPolicies[0], bundle=True, PolicyGroup_DN = accessPolicies[2], user_confirmation = user_confirmation)
        # del_bundle_ports_egp_static_paths(moDir, accessPolicies[2])
    else: #########
        print("==> Port does NOT belong any PC or VPC bundle")
        remove_port_access_policies(moDir, interface_selector_DN = accessPolicies[1], port_block_obj = accessPolicies[0], bundle=False, user_confirmation = user_confirmation)
        del_orphan_ports_egp_static_paths(moDir, node_id, port_name, user_confirmation)

print("\n")
apic_logoff(moDir)




