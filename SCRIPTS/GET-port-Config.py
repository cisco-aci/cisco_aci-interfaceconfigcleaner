from LIBS.AciLibraries import *
import argparse

# Initialize parser
my_parser = argparse.ArgumentParser(description='A script that will display port config \
    (Access Policies & EPG static binding)')
my_parser.add_argument('-H', '--host', type=str, required=True, metavar='', help='APIC url in x.x.x.x or FQDN format and without https://')
my_parser.add_argument('-U', '--user', type=str, required=True, metavar='', help='APIC username')
my_parser.add_argument('-P', '--password', type=str, required=True, metavar='', help='APIC password')
my_parser.add_argument('-L', '--leaf', type=str, required=True, metavar='', help='Leaf ID')
my_parser.add_argument('-I', '--interface', type=str, required=True, metavar='', help='Interface ID, like eth1/1')

args = my_parser.parse_args()


node_id = args.leaf
port_name = args.interface

moDir = apic_logon("https://" + args.host, args.user, args.password)


accessPolicies = get_Port_AccessPolicies(moDir=moDir, leaf_id=node_id, port_name=port_name) # return a list - index 0 is the PORT BLOCK - index 1 is INTERFACE SELECTOR - index 2 is PG

if not accessPolicies: # no access policy found
    print("No Access Policy Found")
    get_orphan_ports_egp_static_paths(moDir, node_id, port_name)
    

elif len(accessPolicies) == 2: # access policy found without PG
        print("Polic Group is not set")

        obj_int_selector = get_obj_from_DN(moDir, accessPolicies[1])
        print(f"Interface Selector: {obj_int_selector[0].name}")

        int_prof_dn = get_parent_from_DN_string(accessPolicies[1])
        int_prof_name = get_obj_from_DN(moDir, int_prof_dn)
        print(f"Interface Profile: {int_prof_name[0].name}")

else: # need to check bundle or orphan
    if is_pg_bundle(accessPolicies[2]) == True:
            print("This is a bundle member port")

            int_prof_dn = get_parent_from_DN_string(accessPolicies[1])
            int_prof_name = get_obj_from_DN(moDir, int_prof_dn)
            print(f"Interface Profile: {int_prof_name[0].name}")

            obj_int_selector = get_obj_from_DN(moDir, accessPolicies[1])
            print(f"Interface Selector: {obj_int_selector[0].name}")

            obj_policy_group = get_obj_from_DN(moDir, accessPolicies[2])
            print(f"Policy Group: {obj_policy_group[0].name}")

            get_port_bundle_epg_static_paths(moDir, accessPolicies[2])


    else: #########
        print("Port does NOT belong to any PC or VPC bundle")

        int_prof_dn = get_parent_from_DN_string(accessPolicies[1])
        int_prof_name = get_obj_from_DN(moDir, int_prof_dn)
        print(f"Interface Profile: {int_prof_name[0].name}")

        
        obj_int_selector = get_obj_from_DN(moDir, accessPolicies[1])
        print(f"Interface Selector: {obj_int_selector[0].name}")

        obj_policy_group = get_obj_from_DN(moDir, accessPolicies[2])
        print(f"Policy Group: {obj_policy_group[0].name}")

        get_orphan_ports_egp_static_paths(moDir, node_id, port_name)

    


print("\n")
apic_logoff(moDir)




