from cobra.mit.access import MoDirectory, ClassQuery
from cobra.mit.session import LoginSession
from cobra.mit.request import ConfigRequest, DnQuery
from cobra.mit.naming import Dn
import re

import requests.packages.urllib3

requests.packages.urllib3.disable_warnings()

def apic_logon(url, username, password):
    my_session = LoginSession(url, username, password)
    modir_local = MoDirectory(my_session)
    modir_local.login()
    return modir_local

def apic_logoff(moDir):
    moDir.logout()

def get_leaf_prof_dn_by_nodeid(moDir, leaf_id):
    cq = ClassQuery('infraNodeBlk')
    result = moDir.query(cq)
    my_results = []

    for key in result:
        if int(key.from_) <= int(leaf_id) <= int(key.to_):
            child = Dn.fromString(format(key.dn))
            my_results.append(format(child.getAncestor(2)))
    return my_results
    # this returns a list of DN as string

def get_int_prof_by_leaf_prof(moDir, leaf_prof):
    cq = ClassQuery('infraRsAccPortP')
    my_result = []
    result = moDir.query(cq)
    for key in result:
        # temp = format(key.dn)  # this is a string
        child = Dn.fromString(format(key.dn))
        parent = format(child.getAncestor(1))
        if parent == leaf_prof:
            my_result.append(key.tDn)
    return(my_result)
    # this returns a list of interface profiles DN (as a string) based attached to a particular switch profile

def get_pg_and_portblock_by_int_prof_from_port_id(moDir, parm1, parm2):
    # print(f"parm2 : {parm2}")
    # print(f"parm1 : {parm1}")
    string_split = parm2.split("/")
    port_id = string_split[1]

    re_filter = re.findall(r'\d+', string_split[0]) # return a list
    slot_id = int(re_filter[0])
    # print(f"port id : {port_id}")
    # print(f"slot id : {slot_id}")
    # print(f"slot id type : {type(slot_id)}")

    
    req = moDir.lookupByClass("infraPortBlk", parentDn=parm1) # querying ports blocks whose parent is our interface profile as parm2 parameter
    my_result = []
    # print(f"req : {req}")
    for key in req:
        # print(f"key.fromCard : {key.fromCard}, key.fromCard : {key.fromCard},")
        if int(key.fromCard) == slot_id and int(key.toCard) == slot_id:
            if int(key.fromPort) <= int(port_id) <= int(key.toPort): # we parse all port block looking for our port id 
                my_result.append(key) # add the PORT BLOCK result in our final list slot 0
                child = Dn.fromString(format(key.dn)) # we build the DN object for the PORT BLOCK
                parent = format(child.getAncestor(1))  # here I got the INTERFACE SELECTOR DN as a string
                my_result.append(parent)
                # print self.parent
                req2 = moDir.lookupByClass("infraRsAccBaseGrp", parentDn=parent)  # here I get the Policy group DN
                if req2:
                    for pg in req2:
                        if pg.tDn:  # this checks whether the PG is set , need to explicitely check tDn as the object always exist
                            my_result.append(format(pg.tDn))
    return my_result
            # return a list - index 0 is the PORT BLOCK - index 1 is INTERFACE SELECTOR - index 2 is PG

def get_Port_AccessPolicies(moDir, leaf_id, port_name):
    accessPolicies = []
    print ("\n#####   Querying port {0} on leaf {1}   #####".format(leaf_id, port_name))
    step1 = get_leaf_prof_dn_by_nodeid(moDir, leaf_id)
    if step1:
        for i in step1:
            step2 = get_int_prof_by_leaf_prof(moDir, i)
            if step2:
                for j in step2:
                    accessPolicies = get_pg_and_portblock_by_int_prof_from_port_id(moDir, j, port_name)
                    if accessPolicies:
                        return(accessPolicies)
                    
def is_pg_bundle(parm):
    pg_class = Dn.fromString(parm)
    if format(pg_class.moClass) == "<class 'cobra.modelimpl.infra.accbndlgrp.AccBndlGrp'>":
        is_bundle = True
        return is_bundle
    elif format(pg_class.moClass) == "<class 'cobra.modelimpl.infra.accportgrp.AccPortGrp'>":
        is_bundle = False
        return is_bundle

# format port id = eth1/1
def get_mo_for_interface(moDir, node_id, port_name):
    cq = ClassQuery('fabricPathEpCont')
    cq.propFilter = 'eq(fabricPathEpCont.nodeId, "{0}")'.format(node_id)
    cq.subtree = 'children'
    cq.subtreeClassFilter = 'fabricPathEp'
    req = moDir.query(cq)
    for key in req[0].children:
        if key.name == port_name:
            return format(key.dn)  # return a string containing the MO for the requested interface

def get_epg_staticpaths_from_int_mo(moDir, parm1):
    cq = ClassQuery('fvRsPathAtt')
    cq.propFilter = 'eq(fvRsPathAtt.tDn, "{0}")'.format(parm1)
    my_result = moDir.query(cq)
    return my_result  # return list of objects

def del_orphan_ports_egp_static_paths(moDir, node_id, port_name, user_confirm):
    interface_MO_DN = get_mo_for_interface(moDir, node_id, port_name)
    if interface_MO_DN:
        #print(f"interface_MO_DN : {interface_MO_DN}")
        epg_static_paths_list = get_epg_staticpaths_from_int_mo(moDir, interface_MO_DN)
        if epg_static_paths_list:
            if user_confirm is False:
                for binding in epg_static_paths_list:
                    delete_mo(moDir, binding)
                    print("==> Deleted Path: {0} -- Encap: {1} -- Mode: {2}".format(binding.dn, binding.encap, binding.mode))
            else:
                print("Following EPG static path marked for deletion:")
                for binding in epg_static_paths_list:
                    print("Path: {0} -- Encap: {1} -- Mode: {2}".format(binding.dn, binding.encap, binding.mode))
                user_input = input("Delete EPG Static Paths ? (Y or N): ")
                if user_input == "Y":
                    for binding in epg_static_paths_list:
                        delete_mo(moDir, binding)
                        print("==> Deleted Path: {0} -- Encap: {1} -- Mode: {2}".format(binding.dn, binding.encap, binding.mode))
                else:
                    exit("Aborting")

        else:
            print("==> No EPG Static Path found")
    else:
        print(f"==> Interface {port_name} on Leaf {node_id} does not exist")

def del_port_bundle_epg_static_paths(moDir, PG_DN, user_confirm):
    target = get_PortBundle_EPG_static_path_target(moDir, PG_DN)
    paths = get_epg_staticpaths_from_int_mo(moDir, target)
    if paths:
        if user_confirm is False:
            for binding in paths:
                delete_mo(moDir, binding)
                print("==> Deleted Path: {0} -- Encap: {1} -- Mode: {2}".format(binding.dn, binding.encap, binding.mode))
        else:
            print("Following EPG static path marked for deletion:")
            for binding in paths:
                print("Path: {0} -- Encap: {1} -- Mode: {2}".format(binding.dn, binding.encap, binding.mode))
            user_input = input("Delete EPG Static Paths ? (Y or N): ")
            if user_input == "Y":
                for binding in paths:
                    delete_mo(moDir, binding)
                    print("==> Deleted Path: {0} -- Encap: {1} -- Mode: {2}".format(binding.dn, binding.encap, binding.mode))
            else:
                exit("Aborting")


    else:
        print("==> No EPG Static Path found")


def get_PortBundle_EPG_static_path_target(moDir, PG_DN):
    PG_name_obj = get_obj_from_DN(moDir, PG_DN)
    cq = ClassQuery('fabricProtPathEpCont')
    cq.subtree = 'children'
    cq.subtreeClassFilter = 'fabricPathEp'
    cq.subtreePropFilter = 'eq(fabricPathEp.name, "{0}")'.format(PG_name_obj[0].name)
    req = moDir.query(cq)
    
    if req[0].numChildren == 0:
        print("==> PolicyGroup Type is PC")
        cq = ClassQuery('fabricPathEpCont')
        cq.subtree = 'children'
        cq.subtreeClassFilter = 'fabricPathEp'
        cq.subtreePropFilter = 'eq(fabricPathEp.name, "{0}")'.format(PG_name_obj[0].name)
        req = moDir.query(cq)
        for key in req[0].children:
            return(key.dn)
    else:
        print("==> Policy Group Type is VPC")
        for key in req[0].children:
            return(key.dn)

def delete_mo(moDir, mo_object):
    if mo_object is not None:
        mo_object.delete()
        config_req = ConfigRequest()
        config_req.addMo(mo_object)
        moDir.commit(config_req)

def get_obj_from_DN(moDir, parm1):
    cq = DnQuery(parm1)
    my_result = moDir.query(cq)
    return my_result  # return list of objects

def get_PortBlk_children_from_IntSelector_obj(moDir, interface_selector_obj):
    PortBlk = []
    cq = DnQuery(interface_selector_obj.dn)
    cq.subtree = 'children'
    cq.subtreeClassFilter = 'infraPortBlk'
    req = moDir.query(cq)
    for key in req[0].children:
        PortBlk.append(key)
    return(PortBlk)

def get_PortBlock_specs(obj):
    fromPort = obj.fromPort
    toPort = obj.toPort
    if fromPort == toPort:
        return('mono')
    else:
        return('range')

def get_Bundle_infraHPortS_members_DN(moDir, PG_DN):
    # we can get infraHPortS relative to a bundle PG
    member_ports_infraHPortS_list=[]
    cq = ClassQuery('infraAccBndlGrp')
    cq.propFilter = 'eq(infraAccBndlGrp.dn, "{0}")'.format(PG_DN)
    cq.subtree = 'children'
    cq.subtreeClassFilter = 'infraRtAccBaseGrp'
    req = moDir.query(cq)
    for key in req[0].children:
        member_ports_infraHPortS_list.append(key.tDn)
    return(member_ports_infraHPortS_list)
    
def remove_port_access_policies(moDir, interface_selector_DN, port_block_obj, bundle, user_confirmation, PolicyGroup_DN=None):
    # we need to the interface selector object
    
    if bundle == True:
        print("==> need to find ALL Interfaces Selectors relative to our Bundle")
        member_ports_infraHPortS_list = get_Bundle_infraHPortS_members_DN(moDir, PolicyGroup_DN)
        
        if user_confirmation is False:
            for item in member_ports_infraHPortS_list:
                member_ports_infraHPortS_obj = get_obj_from_DN(moDir, item)
                print("==> Deleting Interface Selector : {0}".format(member_ports_infraHPortS_obj[0].name))
                delete_mo(moDir,member_ports_infraHPortS_obj[0])
        else:
            print("Following Interface Selectors marked for deletion:")
            for item in member_ports_infraHPortS_list:
                member_ports_infraHPortS_obj = get_obj_from_DN(moDir, item)
                print("Interface Selector : {0}".format(member_ports_infraHPortS_obj[0].name))

            user_input = input("Delete Interface Selectors ? (Y or N): ")
            if user_input == "Y":
                for item in member_ports_infraHPortS_list:
                    member_ports_infraHPortS_obj = get_obj_from_DN(moDir, item)
                    print("==> Deleting Interface Selector : {0}".format(member_ports_infraHPortS_obj[0].name))
                    delete_mo(moDir,member_ports_infraHPortS_obj[0])
            else:
                exit("Aborting")



    elif bundle == False or bundle == None:
        interface_selector_obj = get_obj_from_DN(moDir, interface_selector_DN)
        #since the PG is not PC/VPC we need to know whether the same Interface Selector 
        # was used for more than one orphan port, in that case we can not remove it
        # we need to know whether the Interface Selector has more than one children PortBlock
        PortBlk_list = get_PortBlk_children_from_IntSelector_obj(moDir, interface_selector_obj[0])
        print("==> Interface Selector has {0} children PortBlock".format(len(PortBlk_list)))
        if len(PortBlk_list) > 1:
            exit("==> Can not remove Interface Selector with more than one children PortBlock !!!")
        else:
            # at this stage we know there is only one children PortBlock, but we still need
            # to make sure it is configured with a single port (not a range)
            
            pg_spec = get_PortBlock_specs(port_block_obj)
            if pg_spec == "mono":
                if user_confirmation is False:
                    print("==> Deleting Interface Selector: {0}".format(interface_selector_obj[0].name))
                    delete_mo(moDir, interface_selector_obj[0])
                else:
                    print("Following Interface Selector marked for deletion:")
                    print("Interface Selector: {0}".format(interface_selector_obj[0].name))
                    user_input = input("Delete Interface Selectors ? (Y or N): ")
                    if user_input == "Y":
                        print("==> Deleting Interface Selector: {0}".format(interface_selector_obj[0].name))
                        delete_mo(moDir, interface_selector_obj[0])
                    else:
                        exit("Aborting")

            else:
                exit("==> Cant not remove Interface Selector when PG Type is not PC/VPC and port range > 1 !!!")
    


def get_parent_from_DN_string(dn_string):
	child = Dn.fromString(dn_string)
	parent = child.getAncestor(1)
	return(parent)

 

def get_orphan_ports_egp_static_paths(moDir, node_id, port_name):
    interface_MO_DN = get_mo_for_interface(moDir, node_id, port_name)
    if interface_MO_DN:
        #print(f"interface_MO_DN : {interface_MO_DN}")
        epg_static_paths_list = get_epg_staticpaths_from_int_mo(moDir, interface_MO_DN)
        if epg_static_paths_list:
            print("\n")
            print("Static Paths:")              
            for binding in epg_static_paths_list:
                print("Path: {0} -- Encap: {1} -- Mode: {2}".format(binding.dn, binding.encap, binding.mode))
        else:
                print("==> No EPG Static Path found")
    else:
        print(f"==> Interface {port_name} on Leaf {node_id} does not exist")




def get_port_bundle_epg_static_paths(moDir, PG_DN):
    target = get_PortBundle_EPG_static_path_target(moDir, PG_DN)
    paths = get_epg_staticpaths_from_int_mo(moDir, target)
    if paths:
        print("\n")
        print("Static Paths:")
        for binding in paths:
            print("Path: {0} -- Encap: {1} -- Mode: {2}".format(binding.dn, binding.encap, binding.mode))
        
    else:
        print("==> No EPG Static Path found")


def get_leaf_obj(moDir):
    cq = ClassQuery('fabricNode')
    cq.propFilter = 'eq(fabricNode.role, "leaf")'
    req = moDir.query(cq)
    return req

def get_int_per_leafID(moDir, id):
    cq = ClassQuery('topSystem')
    cq.propFilter = 'eq(topSystem.id, "{0}")'.format(id) 
    cq.subtree = 'children'
    cq.subtreeClassFilter = 'l1PhysIf'
    req = moDir.query(cq)
    return req[0].children
    


