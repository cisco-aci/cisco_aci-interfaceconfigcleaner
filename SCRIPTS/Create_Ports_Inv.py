from LIBS.AciLibraries import *
import argparse, json

# Initialize parser
my_parser = argparse.ArgumentParser(description='A script that will fetch Leaf ID\'s \
    (Access Policies & EPG static binding)')
my_parser.add_argument('-H', '--host', type=str, required=True, metavar='', help='APIC url in x.x.x.x or FQDN format and without https://')
my_parser.add_argument('-U', '--user', type=str, required=True, metavar='', help='APIC username')
my_parser.add_argument('-P', '--password', type=str, required=True, metavar='', help='APIC password')


args = my_parser.parse_args()

moDir = apic_logon("https://" + args.host, args.user, args.password)

my_dict = {}

leaves = get_leaf_obj(moDir)

for x in leaves:
    #print(x.id)
    ports = get_int_per_leafID(moDir, x)

    port_list = []

    for y in ports:
        #print(y.id)
        port_list.append(y.id)
    
    port_list.sort()
    my_dict[x.id] = port_list

print(my_dict)

with open("ACI_port_fabric_inv.json", "w") as outfile:
    json.dump(my_dict, outfile)

